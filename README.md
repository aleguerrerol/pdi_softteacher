# PDI_SoftTeacher
Avance en la implementación de SoftTeacher para segmentación de Salmones

# Implementación

De acuerdo con las instrucciones del repositorio oficial de SoftTeacher https://github.com/microsoft/SoftTeacher . Se indica una versión de Ubuntu pero dado que esta implementación se hará en `macOS 13.0`, las versiones pueden ser más recientes que las recomendadas (en particular Python, se recomienda 3.6 que no se encuentra disponible). 
Se creó un ambiente en MiniConda, y se instalaron los requerimientos con las versiones menos actuales que se encuentran disponibles:

- `Pytorch: 1.11.0`
- `Python: 3.10.8`
- `MMDetection: 2.26`
- `MMCV: 1.17.0`

Luego se comprobó el correcto funcionamiento de MMDetection siguiendo las instrucciones del repositorio oficial https://github.com/open-mmlab/mmdetection haciendo una primera prueba utilizando el modelo preentrenado: `faster_rcnn_r50_fpn_1x_coco`. Dado que la máquina en la que se está implementando no soporta CUDA, fue necesario omitir el uso de GPUs al correr el código, modificando el parámetro ``--devide: cuda:0` (con el número de GPUs disponibles) a `--device cpu`. 

![mmdet_image_test](resources/demo.jpg)
```bash
#en el directorio demo de mmdetection
#prueba con imagen
python3 image_demo.py /Users/ale/mmdetection/demo/demo.jpg /Users/ale/mmdetection/faster_rcnn_r50_fpn_1x_coco.py /Users/ale/mmdetection/faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth --device cpu --out-file result.jpg
```
![mmdet_image_test](resources/result.jpg)

Además, se probaron ejemplos de detección de objetos con vídeo local y streaming.
![mmdet_video_test](resources/video.png)
```bash
python3 demo/video_demo.py demo/demo.mp4 faster_rcnn_r50_fpn_1x_coco.py faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth --device cpu --out output.mp4 --show
```
![mmdet_video_test_result](resources/video_test.png)

